import 'package:flutter/material.dart';
import 'package:gp_calculator/samole.dart';

import 'Screen/HomeScreen/home_screen.dart';
import 'Screen/LoginScreen/login_screen.dart';
import 'Screen/SingupScreen/singup_screen.dart';
import 'Screen/SlaceScreen/slace_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const SlaceScreen(),
    );
  }
}

