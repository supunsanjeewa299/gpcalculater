import 'package:flutter/material.dart';
import 'package:gp_calculator/models/user.dart';
import 'package:gp_calculator/services/database_service/database_helper.dart';

import '../LoginScreen/login_screen.dart';

class SingupScreen extends StatefulWidget {
  const SingupScreen({super.key});

  @override
  State<SingupScreen> createState() => _SingupScreenState();
}

class _SingupScreenState extends State<SingupScreen> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController username = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController confirmpassword = TextEditingController();
  final FocusNode _focusNodeEmail = FocusNode();
  final FocusNode _focusNodePassword = FocusNode();
  final FocusNode _focusNodeConfirmPassword = FocusNode();


  bool _obscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(
                height: 50,
              ),
              const Text("Register",
                  style: TextStyle(fontSize: 40, color: Colors.blueAccent)),

              const Text(
                "Create Your Account",
                style: TextStyle(fontSize: 20, color: Colors.blueAccent),
              ),

              const SizedBox(
                height: 10,
              ),

              // add user name
              TextFormField(
                controller: username,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                    labelText: "User Name",
                    prefixIcon: const Icon(Icons.person),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    )),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "Enter your user name";
                  }
                  return null;
                },
                onEditingComplete: () => _focusNodeEmail.requestFocus(),
              ),

              const SizedBox(
                height: 10,
              ),

              //add email address
              TextFormField(
                controller: email,
                keyboardType: TextInputType.name,
                focusNode: _focusNodeEmail,
                decoration: InputDecoration(
                  labelText: "Email",
                  prefixIcon: const Icon(Icons.email),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "Enter your Email Address";
                  } else if (!(value.contains('@') && value.contains('.'))) {
                    //exampl#$@gmail.com
                    //TODO:validation email in correct manner
                    return "Invalid email";
                  }
                  return null;
                },
                onEditingComplete: () => _focusNodePassword.requestFocus(),
              ),

              const SizedBox(
                height: 10,
              ),

              //add password
              TextFormField(
                controller: password,
                obscureText: _obscurePassword,
                keyboardType: TextInputType.visiblePassword,
                focusNode: _focusNodePassword,
                decoration: InputDecoration(
                    labelText: "Password",
                    prefixIcon: const Icon(Icons.password),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _obscurePassword = !_obscurePassword;
                        });
                      },
                      icon: _obscurePassword
                          ? const Icon(Icons.visibility)
                          : const Icon(Icons.visibility_off),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    )),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "Enter your password";
                  } else if (value.length < 8) {
                    return "Password must be at least 8 character.";
                  }
                  return null;
                },
                onEditingComplete: ()=> _focusNodeConfirmPassword.requestFocus(),
              ),

              const SizedBox(
                height: 10,
              ),

              //add confirm Password

              TextFormField(
                controller: confirmpassword,
                obscureText: _obscurePassword,
                keyboardType: TextInputType.visiblePassword,
                focusNode: _focusNodeConfirmPassword,
                decoration: InputDecoration(
                    labelText: "Password",
                    prefixIcon: const Icon(Icons.password),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _obscurePassword = !_obscurePassword;
                        });
                      },
                      icon: _obscurePassword
                          ? const Icon(Icons.visibility)
                          : const Icon(Icons.visibility_off),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    )),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "Enter your password";
                  } else if (value.length < 8) {
                    return "Password must be at least 8 character.";
                  }
                  return null;
                },
              ),

              const SizedBox(height: 10,),
              //add sing up button
              Column(
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: const Size.fromHeight(50),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        )
                      ),
                      onPressed: ()async{
                        if(_formKey.currentState?.validate()??false){
                          bool isSuccess=await DBhelper.insertUser(User(userName: username.text, email: email.text, password: password.text));
                          if(isSuccess==true){
                            gotoLogin();
                          }
                          else{
                            _showAlertDialog();
                          }
                          _formKey.currentState?.reset();
                        }
                       // _formKey.currentState?.reset();
                        //Navigator.pop(context);
                        //Navigator.pop(context);
                      },

                      child: const Text("Sing up")),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Already have an account?"),
                      TextButton(onPressed: ()=> Navigator.pop(context),
                          child: const Text("Login"))
                    ],
                  )
                ],
              )

            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose(){
    _focusNodeEmail.dispose();
    _focusNodePassword.dispose();
    _focusNodeConfirmPassword.dispose();
    username.dispose();
    email.dispose();
    password.dispose();
    confirmpassword.dispose();
    super.dispose();
  }

  void gotoLogin(){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const LoginScreen()));
  }

  Future<void> _showAlertDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog( // <-- SEE HERE
          title: const Text('oops'),
          content: const SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('User Not registered !'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

