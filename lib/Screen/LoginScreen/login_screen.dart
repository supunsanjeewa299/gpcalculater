import 'package:flutter/material.dart';
import 'package:gp_calculator/services/database_service/database_helper.dart';

import '../HomeScreen/home_screen.dart';
import '../SingupScreen/singup_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  final FocusNode _forcusenodepassword = FocusNode();

  bool _obscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(left: 20, top: 50, right: 20),
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.center,

            children: <Widget>[
              SizedBox(
                height: 256,
                width: 256,
                child: Image.asset('assets/login.jpg'),
              ),

              const SizedBox(
                height: 50,
              ),
              const Text("Welcome",
                  style: TextStyle(fontSize: 40, color: Colors.blueAccent)),

              const SizedBox(
                height: 10,
              ),
              const Text(
                "Login To Your Account",
                style: TextStyle(fontSize: 20, color: Colors.blueAccent),
              ),

              const SizedBox(
                height: 20,
              ),
              //add user name
              TextFormField(
                controller: _username,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                  labelText: "Username",
                  prefixIcon: const Icon(Icons.person_outline),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "Please Enter Your User Name";

                  //else if
                  }
                  return null;
                },
                onEditingComplete: () => _forcusenodepassword.requestFocus(),
              ),

              const SizedBox(
                height: 10,
              ),

              //add password
              TextFormField(
                controller: _password,
                obscureText: _obscurePassword,
                focusNode: _forcusenodepassword,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  labelText: "Password",
                  prefixIcon: const Icon(Icons.password_outlined),
                  suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _obscurePassword = !_obscurePassword;
                        });
                      },
                      icon: _obscurePassword
                          ? const Icon(Icons.visibility)
                          : const Icon(Icons.visibility_off)),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "Please Enter Password";
                  }

                  // else if add value== password
                  return null;
                },
              ),

              const SizedBox(
                height: 50,
              ),

              //add login button
              Column(
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size.fromHeight(50),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        )),
                    onPressed: ()async {
                      bool validation=_formKey.currentState!.validate();

                      if(validation){
                       bool loginStatus=await login(_username.text,_password.text);
                       
                       if(loginStatus){
                         gotoHome();
                       }
                       else{
                         _showAlertDialog();
                       }
                      }
                      else{}

                    },
                    child: const Text("Login"),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Don't have an account?"),
                      TextButton(
                          onPressed: () {
                            _formKey.currentState?.reset();

                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) {
                                return const SingupScreen();
                              }),
                            );
                          },
                          child: const Text("Sing up")),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> login(String userName,String password)async{
    bool result=await DBhelper.getUserauth(userName, password);

    if(result){
      result=true;
    }
    else{
      result=false;
    }
    return result;
  }

  Future<void> _showAlertDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog( // <-- SEE HERE
          title: const Text('oops'),
          content: const SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('check your username or password'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void gotoHome(){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const HomeScreen()));
  }
// --- Button Widget --- //
}
