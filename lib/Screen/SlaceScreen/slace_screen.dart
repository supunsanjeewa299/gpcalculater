import 'dart:async';

import 'package:flutter/material.dart';

import '../LoginScreen/login_screen.dart';

class SlaceScreen extends StatefulWidget {
  const SlaceScreen({super.key});

  @override
  State<SlaceScreen> createState() => _SlaceScreenState();
}

class _SlaceScreenState extends State<SlaceScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Timer(const Duration(seconds: 5),()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const LoginScreen()))
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child:  const Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(
                    backgroundColor: Colors.red,
                  ),
                  SizedBox(width: 20,),
                  Text("Loading")
                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
