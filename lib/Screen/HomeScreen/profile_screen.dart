import 'package:flutter/material.dart';

import '../LoginScreen/login_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_outlined),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,

        children: [

          Padding(
            padding: const EdgeInsets.only(left: 130,top: 20),
            child: Container(
              alignment: Alignment.center,
              width: 150.0,
              height: 150.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(75),
                border: Border.all(
                    color: Colors.white,
                    style: BorderStyle.solid,
                    width: 2.0,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 80,top: 110),
                child: IconButton(icon: const Icon(Icons.camera_alt_outlined,size: 30,),
                  onPressed: () {},),
              ),
            ),
          ),
Container(
  padding: const EdgeInsets.only(right: 170,top: 20),
  child:   Column(
    children: [
      TextButton(onPressed: (){}, child: const Text("Profile")),
      TextButton(onPressed: (){
        Navigator.push(
            context, MaterialPageRoute(builder: (context){
            return const LoginScreen();
        }
        ));
      }, child: const Text("Log out")),

    ],

  ),
)

        ],
      ),
    );
  }
}
