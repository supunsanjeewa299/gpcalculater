import 'package:flutter/material.dart';
import 'package:gp_calculator/Screen/HomeScreen/profile_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // static List<String> result=<String>[
  // "A+", "A", "A-",
  // "B+", "B", "B-",
  // "C+", "C", "C-",
  //"D+", "D", "E"
  //];

  // static List<String> credit=<String>[
  //   "1", "2", "3", "4",
  // ];

  //final List<DropdownMenuItem<String>> _dropDownMenuItems = result.map(
  // (String value) => DropdownMenuItem<String>(
  // value: value,
  // child: Text(value),
  //),
  //)
  //  .toList();
  //final List<PopupMenuItem<String>> _subjectcredit = credit.map(
  //  (String value) => PopupMenuItem<String>(
  // value: value,
  // child: Text(value),
  // ),
  //)
  //.toList();

  //String _btn1SelectedVal = 'A+';
  //String _btn2SelectedVal= "4";

  String dropdownResult = 'A';
  String dropdownCredit = '1';



  // List of items in our dropdown menu
  List result = [
    "A+",
    "A",
    "A-",
    "B+",
    "B",
    "B-",
    "C+",
    "C",
    "C-",
    "D+",
    "D",
    "E"
  ];

  List credit = [
    "1",
    "2",
    "3",
    "4",
  ];
  List dropdownResultList=[];
  List dropdownCreditList=[];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dropdownResultList=result;
    dropdownCreditList=credit;
  }

  // String? selectedValue;
  // List<String> items = [
  //   'Item1',
  //   'Item2',
  //   'Item3',
  //   'Item4',
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {},
        ),
        title: const Text("GP Calculator"),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return const ProfileScreen();
                }),
              );
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "Subject",
                  style: TextStyle(fontSize: 20, color: Colors.deepPurple),
                ),
                Text(
                  "Result",
                  style: TextStyle(fontSize: 20, color: Colors.deepPurple),
                ),
                Text(
                  "Credit",
                  style: TextStyle(fontSize: 20, color: Colors.deepPurple),
                ),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height*0.8,
              child: ListView.separated(itemBuilder: (BuildContext context,int index){
                return columData(index);
              }, separatorBuilder: (BuildContext context,int index){
                return const Divider();
              }, itemCount: 8),
            )

            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceAround,
            //   crossAxisAlignment: CrossAxisAlignment.center,
            //   children: <Widget>[
            //     Text(
            //       "Sub 1",
            //       style: TextStyle(fontSize: 20),
            //     ),
            //     columData(),
            //     Text(
            //       "Sub 1",
            //       style: TextStyle(fontSize: 20),
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
  //Map<String,dynamic> resultValue={"A+":4,"A":4}

  Widget columData(int subID) {
    // const Text('Text("Sub 1",style: TextStyle(fontSize: 20)),');
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(
          subID.toString(),
          style: TextStyle(fontSize: 20),
        ),
        DropdownButton(
          value: dropdownResult,
          icon: const Icon(Icons.keyboard_arrow_down),
          items: result.map((result) {
            return DropdownMenuItem(
              value: result,
              child: Text(result!.toString()),
            );
          }).toList(),
          onChanged: (newValue) {
            setState(() {
              //resultTotal+=resultValue[newValue.toString()]
              dropdownResult = newValue.toString()!;
              //dropdownResultList.add(newValue.toString());
              //dropdownResultList[0]=newValue.toString();
            });
          },
        ),
        DropdownButton(
          value: dropdownCredit,
          icon: const Icon(Icons.keyboard_arrow_down),
          items: credit.map((credit) {
            return DropdownMenuItem(
              value: credit,
              child:  Text(credit.toString()),
            );
          }).toList(),
          onChanged: (newValue) {
            setState(() {
              //totalCredit+=int.parse(newValue);
             // dropdownCreditList[0]=newValue.toString();
              //sdropdownCreditList[subID]=newValue.toString();
               dropdownCredit = newValue.toString()!;
            });
          },
        )
      ],
    );
  }
}
