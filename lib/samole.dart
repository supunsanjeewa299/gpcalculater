import 'package:flutter/material.dart';


class MyListTile extends StatefulWidget {
   const MyListTile({super.key});

  @override
  _MyListTileState createState() => _MyListTileState();
}

class _MyListTileState extends State<MyListTile> {
  String _selectedValue = 'Option 1';
  final List<String> _dropdownValues = ['Option 1', 'Option 2', 'Option 3'];

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const Icon(Icons.person),
      title: const Text('John Doe'),
      subtitle: const Text('johndoe@example.com'),
      trailing: DropdownButton<String>(
        value: _selectedValue,
        items: _dropdownValues.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        onChanged: (String? newValue) {
          setState(() {
            _selectedValue = newValue!;
          });
        },
      ),
      onTap: () {
        // Handle tile tap
        // This function will be called when the user taps on the ListTile
      },
    );
  }
}
