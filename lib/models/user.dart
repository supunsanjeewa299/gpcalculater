class User {
  final String userName;
  final String email;
  final String password;

  User({
    required this.userName,
    required this.email,
    required this.password
});

  Map<String,dynamic> toMap(){
    return {
      'userName':userName,
      'email':email,
      'password':password
    };
  }
}