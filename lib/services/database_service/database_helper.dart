
import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../../models/user.dart';


class DBhelper{
  static Future<Database>initDB() async {
    var dbPath=await getDatabasesPath();
    String path=join(dbPath,'gpDatbase.db');
    return openDatabase(path,version: 1,onCreate: onCreate);
  }

static Future onCreate(Database db, int version) async {
    const sql1='''Create table users(
    id INTEGER primary key autoincrement,
    userName text,
    email text,
    password text,
    isActive INTEGER
    )''';
    await db.execute(sql1);

}

static Future<bool> insertUser(User user) async {
    var result;
    final db=await initDB();
    //isAvaible="SELECT id FROM users Where email=${user.email}"

    result=await db.insert('users', user.toMap(),conflictAlgorithm: ConflictAlgorithm.replace);
    if(result<1){
      return false;
    }
    else{
      return true;
    }
}

static Future<bool>getUserauth(String username, String password) async {
    final db=await initDB();


    var results=await db.rawQuery("select id from users where username='$username' and password='$password'");
    //resultsDemo=db.rawQuery("SELECT id FROM users WHERE userName=? AND password=?",[username,password]);

  if(results.isEmpty){
    return false;
  }
  else{
    return true;
  }
}
}